const laptopsElement= document.getElementById("laptops")
const eleFeedback= document.getElementById("feedback")
const eleSalary= document.getElementById("salary")
const eleLoan= document.getElementById("loan")
const eleBankBalance=document.getElementById("bank-balance")
const eleRepayBtn=document.getElementById("repay_btn")
const eleOutstandingLoan=document.getElementById("outstanding-loan")
let laptops;
let loan=0
async function getComputers() {
    try {
        // Use await to "Wait" for te request to finish
        response = await fetch('https://noroff-komputer-store-api.herokuapp.com/computers');
        laptops=await response.json();
        const option=document.createElement("option")
        option.value=0
        option.text=""
        laptopsElement.add(option)
        for(const computer of laptops){
            const option=document.createElement("option")
            option.value=computer.id
            option.text=computer.title
            laptopsElement.add(option)
        }
    }
    catch(error) {
        console.error('Something went wrong', error);
    }
}
getComputers()

async function OnselectItem(){
    for(const computer of laptops){
            if(document.getElementById("laptops").selectedIndex==0){
                document.getElementById("laptop_info").style.display="none"
                document.getElementById("features-desc").innerHTML=""  
                document.getElementById("features-h2").style.display="none"
            }
        if(computer.id==document.getElementById("laptops").selectedIndex){
            document.getElementById("laptop_info").style.display="flex"
            document.getElementById("features-h2").style.display="block"
            let specs="<ul>"
            for(const spec of computer.specs){
                specs+="<li>"+spec+"\n</li>"
            }
            document.getElementById("features-desc").innerHTML=specs+"</ul>"
            document.getElementById("laptopName").innerHTML=computer.title
            document.getElementById("laptopDesc").innerHTML=computer.description
            document.getElementById("laptopPrice").innerHTML=computer.price
            document.getElementById("comp_img").src=await imageExists(computer)
        }
    }
}
async function imageExists(computer){
    let img_url, mimes=[ "png","jpeg","jpg","gif", "svg"];
    [mimes[mimes.indexOf(computer.image.split(".")[1])] , mimes[0]]= [mimes[0], mimes[mimes.indexOf(computer.image.split(".")[1])]]

    let http = new XMLHttpRequest();
    for (let m of mimes){
        img_url="https://noroff-komputer-store-api.herokuapp.com/"+computer.image.split(".")[0]+"."+m
        http = new XMLHttpRequest();
        http.open('HEAD', img_url, false);
        http.send();
        if(http.status != 404){
            return img_url
        }
    }
    return http.status != 404;
}

function makeMoney(){
    eleFeedback.style.display="none"
    eleSalary.innerHTML=parseInt(eleSalary.innerHTML)+100
}

function bankMoney(){
    eleFeedback.style.display="none"
    if(parseInt(eleOutstandingLoan.innerHTML)==0){
        loan=0
        eleLoan.style.display="none"
        eleBankBalance.innerHTML=parseInt(eleSalary.innerHTML)+
        parseInt(eleBankBalance.innerHTML)
    }else{
        let extra = parseInt(eleOutstandingLoan.innerHTML)-parseInt(eleSalary.innerHTML)*0.10
        if(extra>=0){
            eleBankBalance.innerHTML=parseInt(eleBankBalance.innerHTML)+parseInt(eleSalary.innerHTML)*0.90
            eleOutstandingLoan.innerHTML=parseInt(eleOutstandingLoan.innerHTML)
            -parseInt(eleSalary.innerHTML)*0.10
            if(parseInt(eleOutstandingLoan.innerHTML)==0){
                loan=0
                eleLoan.style.display="none"
                eleRepayBtn.style.display="none"

            }
        }else{
            eleBankBalance.innerHTML=(parseInt(eleBankBalance.innerHTML)+parseInt(eleSalary.innerHTML)*0.90)-extra
            eleOutstandingLoan.innerHTML="0"
            eleLoan.style.display="none"
            eleRepayBtn.style.display="none"
            loan=0
        }
    }
    eleSalary.innerHTML="0"
}
function getLoan(){
    eleFeedback.style.display="none"
    if(loan==0){
        let amount = prompt("Please enter the amount you want to lend:", "");
        if(amount !=null && amount!=""){
            if(parseInt(amount)>parseInt(eleBankBalance.innerHTML)*2){
                eleFeedback.style.display="block"
                eleFeedback.style.color="red"
                eleFeedback.innerHTML="Loan is too big"
            }else{
                loan=parseInt(amount)
                eleRepayBtn.style.display="block"
                eleFeedback.style.display="block"
                eleFeedback.style.color="green"
                eleFeedback.innerHTML="Success"
                eleLoan.style.display="flex"
                eleOutstandingLoan.innerHTML=loan
                eleBankBalance.innerHTML=parseInt(eleBankBalance.innerHTML)+parseInt(amount)
            }
        }else{
            eleFeedback.style.display="block"
            eleFeedback.style.color="red"
            eleFeedback.innerHTML="Canceled"}
    }else{
        eleFeedback.style.display="block"
        eleFeedback.style.color="red"
        eleFeedback.innerHTML="You can only have 1 loan at a time"
    }
}
function repayLoan(){
    let leftOver=parseInt(eleOutstandingLoan.innerHTML)-parseInt(eleSalary.innerHTML)
    if(leftOver<0){
        eleBankBalance.innerHTML=parseInt(eleBankBalance.innerHTML)-leftOver
        eleOutstandingLoan.innerHTML="0"
    }else{
        eleOutstandingLoan.innerHTML=parseInt(eleOutstandingLoan.innerHTML)-
        parseInt(eleSalary.innerHTML)
    }
    if(parseInt(eleOutstandingLoan.innerHTML)<1){
        eleLoan.style.display="none"
        eleRepayBtn.style.display="none"
        loan=0
    }
    eleSalary.innerHTML="0"

}
function buyNow(){
    if(parseInt(document.getElementById("laptopPrice").innerText)>parseInt(eleBankBalance.innerHTML)){
        alert("You are too poor to buy this PC")
    }else{
        eleBankBalance.innerHTML=parseInt(eleBankBalance.innerHTML)-parseInt(document.getElementById("laptopPrice").innerHTML)
        alert("You are now the owner of this PC")
    }
}



